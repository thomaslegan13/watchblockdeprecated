package com.n0grief.WatchBlock;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.n0grief.WatchBlock.EventHandler.Join;
import com.n0grief.WatchBlock.EventHandler.SQLLogger;
import com.n0grief.WatchBlock.Methods.AddFriendCommand;
import com.n0grief.WatchBlock.Methods.FlatLogCreator;
import com.n0grief.WatchBlock.Methods.FriendsCreator;
import com.n0grief.WatchBlock.Methods.FriendsListCommand;
import com.n0grief.WatchBlock.Methods.PurgeBlockCommand;
import com.n0grief.WatchBlock.Methods.PurgePlayerCommand;
import com.n0grief.WatchBlock.Methods.PurgeWorldCommand;
import com.n0grief.WatchBlock.Methods.RemoveFriendCommand;
import com.n0grief.WatchBlock.Methods.UpdateChecker;
import com.n0grief.WatchBlock.SQL.FriendsHandler;
import com.n0grief.WatchBlock.SQL.MYSQL;
import com.n0grief.WatchBlock.SQL.TableCreator;

import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin implements Listener {
	
	public MYSQL SQL;
	public TableCreator tablecreator;
	public FriendsHandler friendshandler;
	public Join join;
	public SQLLogger sqllogger;
	public UpdateChecker updatechecker;
	@Override
	public void onEnable() {
		//Load Config.yml / method for handling configuration queries
		getConfig().options().copyDefaults();
		saveDefaultConfig();
		//Find or create FlatLog file
		FlatLogCreator.setupblocksflatfile();
		//Find or create Friends-List file
		FriendsCreator.setupfriendslist();
		//SQL CONNECTION HANDLER
		this.tablecreator = new TableCreator(this);
		this.friendshandler = new FriendsHandler(this);
		this.SQL = new MYSQL();
		this.updatechecker = new UpdateChecker(this);
		//SQL CONNECTION HANDLER START
		try {
			SQL.connect();
		} catch (ClassNotFoundException | SQLException e) {
			Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] DATABASE DID NOT CONNECT SUCCESSFULLY");
			Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] PLEASE CHECK YOUR MYSQL LOGIN DETAILS IN THE PLUGIN CONFIGUATION FILE!");
			Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] THE PLUGIN WILL NOW DISABLE ITSELF AS NON-SQL LOGGING ISN'T YET SUPPORTED.");
			Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] IF YOU AREN'T USING MYSQL YOU'RE SHIT OUTTA LUCK FOR NOW SORRY.");
			this.getServer().getPluginManager().disablePlugin(this);
		}
		if(SQL.isConnected()) {
			Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] MYSQL DATABASE HAS SUCCESSFULLY BEEN CONNECTED!");
			tablecreator.createFriendsTable();
			Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] FRIENDS TABLE CREATED/LOADED.");
			tablecreator.createBlocksTable();
			Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] BLOCKS TABLE CREATED/LOADED.");
			Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] WARNING: WATCHBLOCK CURRENTLY ONLY SUPPORTS REGULAR BLOCKS, DOORS, SIGNS, ITEMFRAMES, ");
			Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] WARNING: TORCHES, CHESTS, ETC. ARE NOT YET IMPLEMENTED AND WILL NOT BE PROTECTED!");
		//END OF SQL CONNECTION HANDLER
		}
		//Check server version VS Spigot.org version
		if(!updatechecker.isUpdated()){
			Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK] Plugin out of date, if you see this message, please update before posting a bug report.");
		}
		//getServer().getPluginManager().registerEvents(new BlockTracker(), this);
		getServer().getPluginManager().registerEvents(new Join(friendshandler), this);
		getServer().getPluginManager().registerEvents(new SQLLogger(this), this);
		new AddFriendCommand(this);
		new FriendsListCommand(this);
		new RemoveFriendCommand(this);
		new PurgePlayerCommand(this);
		new PurgeBlockCommand(this);
		new PurgeWorldCommand(this);
	}
	
	@Override
	public void onDisable() {
		SQL.disconnect();
	}

}

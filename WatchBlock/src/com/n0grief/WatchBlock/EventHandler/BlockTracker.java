package com.n0grief.WatchBlock.EventHandler;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import net.md_5.bungee.api.ChatColor;

public class BlockTracker implements Listener {
	
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();
		
		Location b_loc = block.getLocation();		
		player.sendMessage(ChatColor.GREEN + "You broke: " + block.getType());
		player.sendMessage(ChatColor.BLUE + "Location: ");
		player.sendMessage(ChatColor.GOLD + "World: " + b_loc.getWorld().getName());
		player.sendMessage(ChatColor.DARK_RED + "X:" + b_loc.getBlockX());
		player.sendMessage(ChatColor.DARK_RED + "Y:" + b_loc.getBlockY());
		player.sendMessage(ChatColor.DARK_RED + "Z:" + b_loc.getBlockZ());
	}	
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();
		
		Location b_loc = block.getLocation();		
		player.sendMessage(ChatColor.GREEN + "You placed: " + block.getType());
		player.sendMessage(ChatColor.BLUE + "Location: ");
		player.sendMessage(ChatColor.GOLD + "World: " + b_loc.getWorld().getName());
		player.sendMessage(ChatColor.DARK_RED + "X:" + b_loc.getBlockX());
		player.sendMessage(ChatColor.DARK_RED + "Y:" + b_loc.getBlockY());
		player.sendMessage(ChatColor.DARK_RED + "Z:" + b_loc.getBlockZ());
	}
}

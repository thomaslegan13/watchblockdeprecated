package com.n0grief.WatchBlock.EventHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import com.n0grief.WatchBlock.SQL.FriendsHandler;

public class Join implements Listener {
	private final FriendsHandler friendshandler;
	public Join(FriendsHandler friendshandler) {
		this.friendshandler = friendshandler;
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		friendshandler.createPlayer(player);
	}
	
}

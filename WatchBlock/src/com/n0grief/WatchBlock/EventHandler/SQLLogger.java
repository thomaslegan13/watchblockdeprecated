package com.n0grief.WatchBlock.EventHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;

public class SQLLogger implements Listener {
	private Main plugin;
	public SQLLogger(Main plugin) {
		this.plugin = plugin;
	}
	public boolean isFriend(int blockx, int blocky, int blockz, String world, Player player) {
		try {
			PreparedStatement ps5 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_blocks WHERE WORLD=? and X=? and Y=? and Z=?");
			ps5.setString(1, world);
			ps5.setInt(2, blockx);
			ps5.setInt(3, blocky);
			ps5.setInt(4, blockz);
			String breaker = player.getName();
			ResultSet results5 = ps5.executeQuery();
			while(results5.next()) {
				String blockowner = results5.getString("NAME");
				try {
					PreparedStatement ps6 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_friends WHERE NAME=? and FRIENDS LIKE ?");
					ps6.setString(1, blockowner);
					ps6.setString(2, "%" + breaker + "%");
					ResultSet results6 = ps6.executeQuery();
					if(results6.next()) {
						return true;
					}
				} catch(SQLException e) {
					e.printStackTrace();
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean isUnsupported(Material blocktype, Player player) {
		if(blocktype.equals(Material.ACACIA_DOOR) 
				|| blocktype.equals(Material.ACACIA_TRAPDOOR) 
				|| blocktype.equals(Material.BIRCH_DOOR) 
				|| blocktype.equals(Material.BIRCH_TRAPDOOR)
				|| blocktype.equals(Material.CRIMSON_DOOR)
				|| blocktype.equals(Material.CRIMSON_TRAPDOOR)
				|| blocktype.equals(Material.DARK_OAK_DOOR)
				|| blocktype.equals(Material.DARK_OAK_TRAPDOOR)
				|| blocktype.equals(Material.IRON_DOOR)
				|| blocktype.equals(Material.IRON_TRAPDOOR)
				|| blocktype.equals(Material.JUNGLE_DOOR)
				|| blocktype.equals(Material.JUNGLE_TRAPDOOR)
				|| blocktype.equals(Material.OAK_DOOR)
				|| blocktype.equals(Material.OAK_TRAPDOOR)
				|| blocktype.equals(Material.SPRUCE_DOOR)
				|| blocktype.equals(Material.SPRUCE_TRAPDOOR)
				|| blocktype.equals(Material.WARPED_DOOR)
				|| blocktype.equals(Material.WARPED_TRAPDOOR)) {
			player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect doors, please use Lockette to protect this!");
			return true;
		}
		else if(blocktype.equals(Material.SAND)) {
			player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect sand as the physics it experiences are unsupported.");
			return true;
		}
		else if(blocktype.equals(Material.GRAVEL)) {
			player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect gravel as the physics it experiences are unsupported.");
			return true;
		}
		else if(blocktype.equals(Material.WATER)
				|| blocktype.equals(Material.WATER_BUCKET)) {
			player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect water as the physics it experiences are unsupported.");
			return true;
		}
		else if(blocktype.equals(Material.LAVA)
				|| blocktype.equals(Material.LAVA_BUCKET)) {
			player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Watchblock will not protect lava as the physics it experiences are unsupported.");
			return true;
		}
		else {
			return false;
		}

	}
	public void removeBlock(int blockx, int blocky, int blockz, String world, Player player) {
		try {
			PreparedStatement ps5 = plugin.SQL.getConnection().prepareStatement("DELETE FROM wb_blocks WHERE WORLD=? and X=? and Y=? and Z=?");
			ps5.setString(1, world);
			ps5.setInt(2, blockx);
			ps5.setInt(3, blocky);
			ps5.setInt(4, blockz);
			ps5.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public boolean isOwner(int blockx, int blocky, int blockz, String world, Player player) {
		try {
			PreparedStatement ps4 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_blocks WHERE WORLD=? and X=? and Y=? and Z=?");
			ps4.setString(1, world);
			ps4.setInt(2, blockx);
			ps4.setInt(3, blocky);
			ps4.setInt(4, blockz);
			String breaker = player.getName();
			ResultSet results4 = ps4.executeQuery();
			while (results4.next()) {
				String blockowner = results4.getString("NAME");
				if(blockowner.equalsIgnoreCase(breaker)) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean blockexists(int blockx, int blocky, int blockz,String world) {
		try {
			PreparedStatement ps3 = plugin.SQL.getConnection().prepareStatement("SELECT * FROM wb_blocks WHERE WORLD=? and X=? and Y=? and Z=?");
			ps3.setString(1, world);
			ps3.setInt(2, blockx);
			ps3.setInt(3, blocky);
			ps3.setInt(4, blockz);
			ResultSet results1 = ps3.executeQuery();
			if(results1.next()) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();
		Location b_loc = block.getLocation();
		String world = b_loc.getWorld().getName();
		Integer blockx = b_loc.getBlockX();
		Integer blocky = b_loc.getBlockY();
		Integer blockz = b_loc.getBlockZ();
		if(player.hasPermission("watchblock.bypass")) {
			removeBlock(blockx, blocky, blockz, world, player);
			event.setCancelled(false);
		}
		else if(blockexists(blockx, blocky, blockz, world)) { 
			if(isOwner(blockx, blocky, blockz, world, player)) {
				event.setCancelled(false);
				removeBlock(blockx, blocky, blockz, world, player);
			}
			else if(!isOwner(blockx, blocky, blockz, world, player)) {
				if(isFriend(blockx, blocky, blockz, world, player)) {
					event.setCancelled(false);
					removeBlock(blockx, blocky, blockz, world, player);
				}
				else {
					event.setCancelled(true);
					player.sendMessage(ChatColor.RED + "[WATCHBLOCK] Someone else owns that block!");
				}
			}
		}
		else if(!blockexists(blockx, blocky, blockz, world)) {
			event.setCancelled(false);
		}
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		Block block = event.getBlock();
		Material blocktype = event.getBlock().getType();
		Player player = event.getPlayer();
		UUID uuid = player.getUniqueId();
		Location b_loc = block.getLocation();
		String world = b_loc.getWorld().getName();
		Integer blockx = b_loc.getBlockX();
		Integer blocky = b_loc.getBlockY();
		Integer blockz = b_loc.getBlockZ();
		if(isUnsupported(blocktype,player)) {
			event.setCancelled(false);
		}
		else if(blockexists(blockx,blocky,blockz,world)) {
			player.sendMessage(ChatColor.RED + "[WATCHBLOCK] The SQL server thinks this block is already registered to a player??? Possible SQL logging issue.");
			player.sendMessage(ChatColor.RED + "[WATCHBLOCK] If you see this error message the plugin may not be working right, contact server admin.");
			event.setCancelled(true);
		}
		else {
			event.setCancelled(false);
			try {
				PreparedStatement ps2 = plugin.SQL.getConnection().prepareStatement("INSERT INTO wb_blocks" + "(NAME,UUID,WORLD,X,Y,Z) VALUES(?,?,?,?,?,?)");
				ps2.setString(1, player.getName());
				ps2.setString(2, uuid.toString());
				ps2.setString(3, world);
				ps2.setInt(4, blockx);
				ps2.setInt(5, blocky);
				ps2.setInt(6, blockz);
				ps2.executeUpdate();
			}
			catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
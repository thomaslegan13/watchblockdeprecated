package com.n0grief.WatchBlock.SQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.bukkit.plugin.Plugin;
import com.n0grief.WatchBlock.Main;

public class MYSQL {
	//PULLING MYSQL LOGIN INFO AND DEFINING IT BASED OF CONTENT OF CONFIG.YML
	Plugin plugin = Main.getPlugin(Main.class);
	private String host = plugin.getConfig().getString("Host");
	private String port = plugin.getConfig().getString("Port");
	private String database = plugin.getConfig().getString("Database");
	private String username = plugin.getConfig().getString("Username");
	private String password = plugin.getConfig().getString("Password");
	
	private Connection connection;
	//CHECK SQL CONNECTION
	public boolean isConnected() {
		return (connection == null ? false : true);
	}
	//IF SQL IS NOT CONNECTED THIS CODE IS RAN TO CONNECT TO IT
	public void connect() throws ClassNotFoundException, SQLException {
		if(!(isConnected())) {
			connection = DriverManager.getConnection("jdbc:mysql://" +
					host + ":" + port + "/" + database + "?useSSL=false",
					username, password);
		}
	}
	//SQL DISCONNECTION CODE
	public void disconnect() {
		if (isConnected()) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public Connection getConnection() {
		return connection;
	}
}
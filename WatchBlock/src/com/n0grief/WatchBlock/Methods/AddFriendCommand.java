package com.n0grief.WatchBlock.Methods;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.n0grief.WatchBlock.Main;
import net.md_5.bungee.api.ChatColor;

public class AddFriendCommand implements CommandExecutor {
	private Main plugin;
	public AddFriendCommand(Main plugin) {
		this.plugin = plugin;
		plugin.getCommand("wallow").setExecutor(this);
	}
	public void wallowSQL(Player player, String args) {
		try {
			PreparedStatement ps1 = plugin.SQL.getConnection().prepareStatement("UPDATE wb_friends SET FRIENDS = CONCAT(FRIENDS,?) WHERE NAME=?");
			ps1.setString(1, " " + args);
			ps1.setString(2, player.getName());
			ps1.executeUpdate();
			player.sendMessage(ChatColor.GREEN + "[WATCHBLOCK] " + args + " has been added to your friendslist!");
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players in-game may execute this command!");
			return true;
		}
		Player player = (Player) sender;
		if (player.hasPermission("watchblock.addfriend")) {
			if(args.length == 1) {
				wallowSQL(player, args[0]);
			}
			else {
				player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You have either specified too many or too few friends to add. 1 at a time please.");
			}
		}
		if(!player.hasPermission("watchblock.addfriend")) {
		player.sendMessage(ChatColor.RED + "[WATCHBLOCK] You do not have permission to use that command!");
		Bukkit.getLogger().info(ChatColor.RED + "[WATCHBLOCK]" + player.getName() + " was denied access to " + cmd.getName());
		}
		return false;
	}
}
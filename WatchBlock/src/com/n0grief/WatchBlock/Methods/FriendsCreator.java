package com.n0grief.WatchBlock.Methods;

import java.io.File;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import net.md_5.bungee.api.ChatColor;

public class FriendsCreator {
	private static File file;
	private static FileConfiguration customFile;
	
	public static void setupfriendslist() {
		file = new File(Bukkit.getServer().getPluginManager().getPlugin("WatchBlock").getDataFolder(),"friends.yml");
				if (!file.exists()) {
					try {
						file.createNewFile();
						Bukkit.getLogger().info(ChatColor.YELLOW + "[WATCHBLOCK] A NEW FRIENDS-LIST FILE WAS CREATED!");
					} catch (IOException e) {
					}
				}
				customFile = YamlConfiguration.loadConfiguration(file);
				Bukkit.getLogger().info(ChatColor.GREEN + "[WATCHBLOCK] FRIENDS-LIST FILE WAS SUCCESSFULLY LOADED!");
				//FINDS/GENERATES THE FRIENDSLIST FILE
	}
	public static FileConfiguration get() {
		return customFile;
	}

}
